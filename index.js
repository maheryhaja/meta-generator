const fs = require('fs');
const { mainModule } = require('process');
const readline = require('readline');

const ORIGIN_FOLDER = './test';
const DESTINATION_FOLDER = './output';

const REPLACEMENTS = [
    {
        in: 'delivery-zones',
        out: 'delivery-costs'
    },
    {
        in: 'delivery-zone',
        out: 'delivery-cost'
    },
    {
        in: 'DELIVERY_ZONES',
        out: 'DELIVERY_COSTS'
    },
    {
        in: 'DELIVERY_ZONE',
        out: 'DELIVERY_COST',
    },
    {
        in: 'deliveryZones',
        out: 'deliveryCosts'
    },
    {
        in: 'deliveryZone',
        out: 'deliveryCost'
    },
    {
        in: 'DeliveryZones',
        out: 'DeliveryCosts'
    },
    {
        in: 'DeliveryZone',
        out: 'DeliveryCost'
    }

];

const isDirectory = path => fs.statSync(path).isDirectory();

const browse = (path, actionForFile, actionForDirectory) => {
    if (isDirectory(path)) {
        actionForDirectory(path);
        fs.readdirSync(path).forEach(file => browse(`${path}/${file}`, actionForFile, actionForDirectory));
    } else {
        actionForFile(path);
    }
}

const processFile = async (filePath, actionPerLine) => {
    const filestream = fs.createReadStream(filePath);
    const rl = readline.createInterface({ input: filestream });
    for await (const line of rl) {
        actionPerLine(line);
    }
}

const createFileIfNotExists = (path) => {
    if (!fs.existsSync(path)) {
        fs.writeFileSync(path, '');
    }
}

const appendToFile = (path, line, addNewLine = true) => {

    fs.appendFileSync(path, `${line}${addNewLine ? '\n' : ''}`);
};

const replaceInFile = async (path, replacements, outputPath) => {
    createFileIfNotExists(outputPath);
    await processFile(path, line => {
        const newLine = replacements.reduce((acc, cur) => acc.split(cur.in).join(cur.out), line);
        appendToFile(outputPath, newLine);
    });
}

const replaceInsindePath = ({ path, inputModel, outputModel, inputDirectoryPath, outPutDirectoryPath }) => path.replace(inputDirectoryPath, outPutDirectoryPath).split(inputModel).join(outputModel);

createDirectory = async (path) => {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }
}

const eachFile = (originFolder, action) => browse(originFolder, action, () => { });
const eachDirectory = (originFolder, action) => browse(originFolder, () => { }, action)


const isDirectoryEmpty = path => fs.readdirSync(path).length === 0;

const main = (inputModel, outputModel, inputFolder, outputFolder) => {
    createDirectory(outputFolder);

    if (!isDirectoryEmpty(outputFolder)) {
        console.log('Output Directory is not empty: ');
    } else {

        const pathReplacementParam = path => ({ path, inputModel, outputModel, inputDirectoryPath: inputFolder, outPutDirectoryPath: outputFolder });



        eachDirectory(inputFolder, dir =>
            createDirectory(replaceInsindePath(pathReplacementParam(dir)))
        );

        eachFile(inputFolder, async file => replaceInFile(file, REPLACEMENTS, replaceInsindePath(pathReplacementParam(file))))
    }
}

main('delivery-zone', 'delivery-cost', '/home/maheryhaja/project/dreamslab/sit/ng-sit-admin/src/app/delivery-zone', './delivery-cost');